
from celery import Celery

app = Celery('calc_config', broker='redis://127.0.0.1/0', backend='redis://127.0.0.1:6379', include=['calc'])
