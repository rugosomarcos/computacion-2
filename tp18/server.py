#!/usr/bin/python3
import asyncio
import getopt
import sys
import re
import time

import calc

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'h:p:')

except:
    print('INVALID ARGUMENTS')
    sys.exit()
for (opt, arg) in opts:
    if opt == "-h":
        ip_server = arg
    elif opt == "-p":
        port = int(arg)


def formatting(data):

    format_data = data.decode('utf-8')
    format_data = re.sub('\s+', ' ', format_data)
    return format_data


async def handle(reader, writer):

    data = await reader.read(100)
    formatting(data)
    data2 = await reader.read(100)
    formatting(data2)
    data3 = await reader.read(100)
    print(data,data2,data3)


    if formatting(data3) == 'sum':

        result = calc.addition.delay(int(data), int(data2))
        time.sleep(1)
        if result.ready():

            writer.write(str(result.get()).encode('utf-8'))
            print(("Result returned"))

    elif formatting(data3) == 'res':
        result = calc.subst(int(data), int(data2))
        time.sleep(1)
        if result.ready():
            writer.write(str(result.get()).encode('utf-8'))
            print(("Result returned"))

    elif formatting(data3) == 'mul':
        result = calc.multiplication.delay(int(data), int(data2))
        time.sleep(1)
        if result.ready():
            writer.write(str(result.get()).encode('utf-8'))
            print(("Result returned"))

    elif formatting(data3) == 'div':
        result = calc.division.delay(int(data), int(data2))
        time.sleep(1)
        if result.ready() & int(data2) != 0:
            writer.write(str(result.get()).encode('utf-8'))
            print("Result returned")
        else:
            print(str("The denominator cant be equal to ZERO"))


    await writer.drain()

    writer.close()


async def main():
    server = await asyncio.start_server(
        handle, ip_server, port)

    print('Serving on IP: ', {ip_server})

    async with server:
        await server.serve_forever()

asyncio.run(main())
