#!/usr/bin/python3
import re
import asyncio
import getopt
import sys
import time

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'h:p:o:n:m:')

except asyncio:
    print('INVALID ARGUMENTS')

    sys.exit()


for (opt, arg) in opts:
    if opt == "-h":
        ip_server = arg
    elif opt == "-p":
        port = int(arg)
    elif opt == "-o":
        operation = arg
    elif opt == "-n":
        op_1 = int(arg)
    elif opt == "-m":
        op_2 = int(arg)


async def tcp_client():
    reader, writer = await asyncio.open_connection(
        ip_server, port)

    writer.write(str(op_1).encode('utf-8'))
    print("Sending operator 1...")
    time.sleep(1)
    writer.write(str(op_2).encode('utf-8'))
    print("Sending operator 2...")
    time.sleep(1)
    writer.write(operation.encode('utf-8'))
    print("Sending operation...")
    time.sleep(1)
    await writer.drain()

    result = await reader.read(10)
    time.sleep(1)
    format_data = result.decode('utf-8')
    format_data = re.sub('\s+', ' ', format_data)

    if operation == 'sum':
        print("ADDING...")
        print(op_1, '+', op_2, '=', int(format_data))

    elif operation == 'res':
        print("SUBSTRACTING...")
        print(int(format_data))

    elif operation == 'mul':
        print("MULTIPLICATING...")
        print(op_1, 'x', op_2, '=', int(format_data))

    elif operation == 'div' and op_2 != 0:
        print("DIVIDING...")
        print(op_1, '/', op_2, '=', float(format_data))
    else:
        print(str("The denominator cant be equal to ZERO"))

    print('Close the connection')
    writer.close()
    await writer.wait_closed()


asyncio.run(tcp_client())


