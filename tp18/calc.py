import errno
from calc_config import app


@app.task
def addition(a, b):
    return a+b


@app.task
def subst(a, b):
    return a-b


@app.task
def multiplication(a, b):
    return a*b


@app.task
def division(a, b):
    if b != 0:
        return a/b
    else:
        return str(ZeroDivisionError)
