#!/usr/bin/python3
import sys
import getopt

def main():

    try:
        opt, args = getopt.getopt(sys.argv[1:], 'o:n:m:', ['operation', 'number1', 'number2'])
        # el : indica que luego debe recibir algo luego de o,n y m
    except getopt.GetoptError as e:
        print("INPUT ERROR")
        exit()
    print('ARGUMENTS  :', sys.argv[1:])

    for (op, ar) in opt:
        if op == '-o':
            operation = ar
        elif op == '-n':
            num1 = int(ar)
        elif op == '-m':
            num2 = int(ar)
        else:
            print("Invalid input")


    print('OPERATION:', operation)
    print('NUMBER 1 :', num1)
    print('NUMBER 2 :', num2)

    if (operation == '+'):
        print(num1, '+', num2, '=', num1+num2)
    elif(operation == '-'):
        print(num1, '-', num2, '=', num1-num2)
    elif (operation == '*'):
        print(num1, 'x', num2, '=', num1*num2)
    elif (operation == '/'):
        print(num1, '/', num2, '=', num1/num2)
    else:
        print("ERROR")

if __name__ == '__main__':
    main()

