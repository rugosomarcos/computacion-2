#!/usr/bin/python3
import sys
import getopt



def main():

    try:
        opt, args = getopt.getopt(sys.argv[1:], 'i:o:')
        # ejercicio-2.py -i archivo_existente.txt -o archivo_nuevo.txt

    except getopt.GetoptError as e:
        print("INPUT ERROR",str(e))
        exit()


    for (op, ar) in opt:
        if op == '-i':
            file = open(ar, 'r')            #Abrimos el archivo
            lines = file.readlines()        #Leemos las lineas y las guardamos
        elif op == '-o':
            file2 = open(ar, 'w')
            file2.writelines(lines)
            print('The file', sys.argv[4], 'was modified succesfully')
        else:
            print("Invalid input")

if __name__ == '__main__':
    main()