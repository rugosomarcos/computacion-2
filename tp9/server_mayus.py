#!/usr/bin/python3
import socket
import sys
import getopt

try:
    opt, args = getopt.getopt(sys.argv[1:], 'p:', ['port'])

except getopt.GetoptError as e:
    print("INPUT ERROR")
    exit()
print("Waiting for connections...")

for (op, ar) in opt:
    if op == '-p':
        port = int(ar)

# create a socket object
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# bind to the port
serversocket.bind(('', port))

serversocket.listen(1)

clientsocket, addr = serversocket.accept()

while True:

    data = clientsocket.recv(512)
    print("Client : %s is connected to the server " % str(addr))
    print("Message received: " + data.decode("ascii"))
    received_msg = data.decode("ascii")
    reply_msg = received_msg.upper()
    clientsocket.send(reply_msg.encode('ascii'))
    print("Reply message: "+reply_msg)
    print("Closing Connection")
    exit()
    serversocket.close()

