#!/usr/bin/python3

import socket
import sys
import getopt

try:
    opt, args = getopt.getopt(sys.argv[1:], 'a:p:', ['address', 'port'])

except getopt.GetoptError as e:
    print("INPUT ERROR")
    exit(1)

for (op, ar) in opt:
    if op == '-a':
        address = ar
    elif op == '-p':
        port = int(ar)

# create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connection to hostname on the port.
s.connect((address, port))

while True:

    msg = input('Enter message to send : ')
    s.send(msg.encode('ascii'))
    # Receive no more than 1024 bytes
    msg_s = s.recv(1024)
    print('Server replied from the server : ' + msg_s.decode("ascii"))
    print("Closing connection")
    s.close()
    exit()



