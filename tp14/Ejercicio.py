#!/usr/bin/python3
import sys
import getopt
import random
import string
import os
import time
import multiprocessing as mp




def options():
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'n:f:r:', [])
        return opts
    except getopt.GetoptError as err:
        print('ERROR : ARGUMENTOS INVALIDOS ')
        exit()


def process(r, l, f):
    char = random.choice(string.ascii_uppercase)
    file = open(f, "a")
    l.acquire()
    for i in range(int(r)):
        time.sleep(1)
        file.write(char)
        file.flush()
        print("PID:", os.getpid(), "Escribio:", char)
    l.release()
    file.close()


def main():
    if len(sys.argv[1:]) > 1:
        opts = options()
        for (opt, arg) in opts:
            if opt == "-n":
                n_proc= int(arg)
                print("\n-------------------------------------------------\n")
                print("Cantidad de procesos: ", n_proc)
            if opt == "-f":
                file = arg
                print("Archivo: ", file)
                print("\n-------------------------------------------------\n")
            if opt == "-r":
                n_iter = int(arg)
                print("Cantidad de iteraciones", n_iter)
        lock = mp.Lock()
        for i in range(n_proc):
            p = mp.Process(target=process, args=(n_iter, lock, file))
            p.start()

        for i in range(n_proc):
            p.join()

    else:
        print("ERROR:ARGUMENTOS INVALIDOS")



if __name__ == '__main__':
    main()
