#!/usr/bin/python3
import socket
import threading
import datetime
MAX_SIZE = 512
KEY = "12135"

TODAY = datetime.datetime.now().strftime("%d-%m-%Y_%H:%M:%S")


def th_server(sock_full):
    name = "_"
    key = "_"
    email = "_"
    sock, addr = sock_full
    print("Launching thread... addr: %s" % str(addr))
    exit = False
    ip = str(addr)
    stage = 0
    while True:
        msg = sock.recv(MAX_SIZE).decode()
        if msg[0:5] == "hello":
            if stage == 0:
                name = msg[6:]
                resp = "200 - OK"
                stage += 1
            else:
                resp = "400 - OUT OF SECUENCE"
        elif msg[0:5] == "email":
            email = msg[6:]
            if stage == 1:
                email = msg[6:]
                resp = "200 - OK"
                stage += 1
            else:
                resp = "400 - OUT OF SECUENCE"
        elif msg[0:3] == "key":
            if stage == 2:
                key = msg[4:]
                if key!= KEY:
                    resp = "404 - WRONG KEY"
                else:
                    resp = "200 - CORRECT KEY"
                    stage += 1
            else:
                resp = "400 - OUT OF SECUENCE"
        elif msg[0:4] == "exit":
            resp = "200 - GOOD BYE"
            exit = True
        else:
            resp = "500"

        sock.send(resp.encode("ascii"))
        if exit:
            data = "%s|%s|%s|%s|%s" % (TODAY, name, email, key, ip)
            data = data.replace('\n', '').replace('\r', '')
            print(data)
            sock.close()
            break


# create a socket object
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

host = ""
port = 4000

# bind to the port
serversocket.bind((host, port))

# queue up to 5 requests
serversocket.listen(5)

while True:
    # establish a connection
    clientsocket = serversocket.accept()

    print("Got a connection from %s" % str(clientsocket[1]))

    th = threading.Thread(target=th_server, args=(clientsocket,))
    th.start()

