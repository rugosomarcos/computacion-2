#!/usr/bin/python3
import sys
import getopt
import socket

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Error creating socket')
    sys.exit()

(opts, args) = getopt.getopt(sys.argv[1:], "h:p:")

for (opt, arg) in opts:
    if opt == "-h":
        host = arg
    if opt == "-p":
        port = int(arg)

s.connect((host, port))

name = "hello|" + (input('Name: '))
s.send(name.encode('ascii'))
msg = s.recv(1024)
print('Server reply : ' + msg.decode("ascii"))

email = "email|" + (input('Email: '))
s.send(email.encode('ascii'))
msg = s.recv(1024)

print('Server reply : ' + msg.decode("ascii"))

key = "key|" + (input('Key: '))
s.send(key.encode('ascii'))
msg = s.recv(1024)

print('Server reply : ' + msg.decode("ascii"))

exit = "exit"
s.send(exit.encode('ascii'))
msg = s.recv(1024)

print('Server reply : ' + msg.decode("ascii"))
