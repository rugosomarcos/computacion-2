#!/usr/bin/python3

import socket
import time

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = "localhost"
port = 8000
sock.connect((host, port))
while True:
    msg = input("CMD:")
    sock.send(msg.encode('ascii'))
    time.sleep(10)
    print(sock.recv(1024).decode())
    exit()
