#!/usr/bin/python3

import socket
import getopt
import sys
import socketserver
import threading
import subprocess

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'm:')

except socket.error:
    print('ERROR')
    sys.exit()

for (opt, arg) in opts:
    if opt == "-m":
        method = arg


class Handler(socketserver.BaseRequestHandler):

    def handle(self):

        try:
            data = self.request.recv(1024)
            print("Message received")
            process = subprocess.Popen([data], shell=True, universal_newlines=True, stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            stdout, stderr = process.communicate()
            if process.returncode == 0:
                if stdout != 0:
                    msg = "\nOk \n" + stdout
                    self.request.send(msg.encode('ascii'))
                elif stdout == 0:
                    msg = "ERROR: \n" + stderr
                    self.request.send(msg.encode('ascii'))

            else:
                msg = "ERROR: \n" + stderr
                self.request.send(msg.encode('ascii'))

        except UnicodeEncodeError:
            msg = b' Invalid Command'
            self.request.send(msg)
        return


class ForkingEchoServer(socketserver.ForkingMixIn,
                        socketserver.TCPServer,
                        ):
    pass


class ThreadedEchoServer(socketserver.ThreadingMixIn,
                         socketserver.TCPServer,
                         ):
    pass


def main():

    address = ('localhost', 8000)
    if method == 'p':

        print("Using forking:")

        server = ForkingEchoServer(address, Handler)
        ip, port = server.server_address

        t = threading.Thread(target=server.serve_forever)
        t.setDaemon(True)
        t.start()

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ip, port))
        print("WORKING IN PORT: ", port)
        s.recv(1024)

    elif method == 't':
        print("Using threading:")
        server = ThreadedEchoServer(address, Handler)
        ip, port = server.server_address

        t = threading.Thread(target=server.serve_forever)
        t.setDaemon(True)
        t.start()

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ip, port))
        print("WORKING IN PORT: ", port)
        s.recv(1024)


if __name__ == '__main__':
    main()
