# PROYECTO FINAL COMPUTACIÓN 2

Para instalar el proyecto y que funcione correctamente debemos seguir los siguientes pasos:

# INSTRUCCIONES PARA CLONAR REPOSITORIO

- Instalar git con las instrucciones adecuadas según el sistema operativo que posea https://git-scm.com/download

- En el repositorio de git le va aparecer un boton "code" donde va a encontrar el link para poder clonarlo

- Posicionarse en el directorio donde desee clonar el repositorio, abrir la terminar y colocar lo siguiente:

	En https:
	
    ```
    git clone https://gitlab.com/rugosomarcos/computacion-2.git
    ```
	En ssh:
    ```
	git clone git@gitlab.com:rugosomarcos/computacion-2.git
    ```

# INSTRUCCIONES PARA INSTALAR LAS LIBRERÍAS

Para instalar las librerías necesarias para que funcione el proyecto, debemos seguir los siguientes pasos indicados a continuación:

Primero crearemos un entorno virtual de la siguiente manera:

    ```
    python3 -m venv venv
    ```
Luego, procedemos a activarlo:

    ```
    source venv/bin/activate
    ```
A continuación, instalamos las librerias necesarias para poder hacer funcionar el proyecto:

    ```
    pip install -r requirements.txt
    ```
    
Para desactivar el entorno virtual, lo hacemos mediante:

    ```
    deactivate
    ```

# INSTRUCCIONES PARA INSTALAR EL PROYECTO UTILIZANDO DOCKER

Luego de clonar el repositorio, ubicarse dentro utilizando el comando cd, luego:

Construir la imagen de docker ejecutando en la terminal:

```
docker build -t nombre-de-la-imagen .
```

Levantar mediante el siguiente comando la imagen creada previamente, indicando el puerto definido dentro del archivo config.env, en nuestro caso por defecto es el 1235

```
docker run -it -p 1235:1235 nombre-de-la-imagen
```

Para ver los historiales de chat de privado y broadcast se debe ejecutar el siguiente comando:

Para el historial de los chat privado:
```
docker exec nombre-de-la-imagen cat chat_records/history_privates.txt
```

Para el historial de broadcast:
```
docker exec nombre-de-la-imagen cat chat_records/history_broadcasts.txt
```