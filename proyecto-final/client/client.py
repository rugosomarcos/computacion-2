#!/usr/bin/python3
import asyncio
import getopt
import sys, os
import configparser
from colorama import Fore,Back,Style
from tqdm import tqdm
import time

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'f:')


except:
    print('Missing arguments, re-run')
    sys.exit()

for (opt, arg) in opts:
    if opt == "-f":
        config_path = os.path.join(f'../{arg}')
        config = configparser.ConfigParser()
        config.read(config_path)
        host = config.get('DEFAULT', 'host')
        port = config.getint('DEFAULT', 'port')



async def handle_input(writer):
    while True:
        message = await asyncio.get_running_loop().run_in_executor(None, input)
        message = message.strip() 
        writer.write(message.encode())
        await writer.drain()
        
async def main():
    
    reader, writer = await asyncio.open_connection(host, port)
    username = await asyncio.get_running_loop().run_in_executor(None, input, "Enter your username: ")
    username = username.strip()
    writer.write(username.encode())
    await writer.drain()
    asyncio.create_task(handle_input(writer))
    loading()
    menu()
    while True:
        data = await reader.read(1024)
        message = data.decode().strip()
        if message == 'Already exist, try with another':
            print(message)
            print("Press enter to try again")
            nick = input("Nickname: ")
            writer.write(f"{nick}".encode())
            data = await reader.read(1024)
            message = data.decode().strip()
        if message == 'Disconnected':
            print("You leave the chat")
            sys.exit()
            
        if message :
            print(message)
        else:
            break
    writer.close()

def loading():
    bar = tqdm(total=100, desc='Loading chat...', position=0, leave=True)
    for i in range(100):
        time.sleep(0.005)
        bar.update(1)
    

def menu():
    os.system("clear")
    print(Back.LIGHTWHITE_EX + Fore.BLACK + "\n------------ Welcome to the chat -------------------------")
    print("MENU:                                                    -")
    print("\t 1. Type @user to chat with another user         -")
    print("\t 2. Type /list to see who is online right now    -")
    print("\t 2. Type /exit to leave the chatroom             -")

    print("----------------------------------------------------------" + Style.RESET_ALL)


asyncio.run(main())


