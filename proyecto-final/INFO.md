# INFO

En este archivo se detallarán las justificaciones de cada decisión tomada en base a distintas herramientas o librerias utilizadas en la cátedra de Computación 2. Es de gran importancia comentar que este archivo se centrará en la base fundamental del funcionamiento del proyecto.


# LIBRERÍA GETOPT

Para la ejecución del servidor como de cada cliente, inicialmente especificabamos internamente en cada código, con el tiempo decidimos optar por el uso de getopt donde, mediante la creación del archivo config.env, logramos incluir el puerto y host dentro de este archivo, logrando una configuración mas eficiente que ahorra pasos.
De tal manera, al iniciar la ejecución de cada cliente o servidor debemos incluir como parámetro -f este archivo.


# IMPLEMENTACIÓN DE ASYNCIO

Inicialmente este proyecto fue pensado para realizarse mediante el uso de la librería threading, nos encontramos con los siguientes impedimentos que determinaron el uso de asyncio como librería fundamental para la construcción de este trabajo. Luego de realizar las debidas investigaciones, junto con numerosas pruebas efectuadas, detectamos lo siguiente:

1. En primera parte, utilizando el módulo threading nos encontramos con grandes problemas para establecer una comunicación adecuada entre los clientes, donde, al no haber implementado mecanismos para el manejo de entrada y salida de cada usuario, aparecían defectos como puede ser que el servidor manejaba los mensajes en orden de entrada, y eran enviados desordenados en algunos casos. En conclusión, detectamos que las tareas se ejecutaban secuencialmente y una tarea no se podía ejecutar hasta que la tarea anterior haya finalizado, por lo que asumimos la necesidad de un modelo de programación asíncrono y no secuencial.
2. Escalabilidad: estimamos que el módulo asyncio es más escalable que threading debido a que es capaz de manejar una gran cantidad de conexiones simultáneas de manera más eficiente.
3. Detectamos que en asyncio una tarea no se bloquea cuando realiza una operación de entrada/salida y el control se devuelve al bucle de eventos principal que se encarga de ejecutar otras tareas mientras la operación de entrada/salida se completa en segundo plano. Siendo este un factor relevante ya que nuestro proyecto incluye numerosas tareas de entrada y salida, manejadas desde el servidor. 

Es importante comentar que el módulo threading podría haber sido utilizado, pero por las razones mencionadas anteriormente se decidió optar por el módulo asyncio, asumiendo que era la librería mas adecuada y de facil uso para este caso.


# COLORAMA, TQDM Y DATETIME

Estas librerías fueron implementadas para dar detalles estéticos al proyecto, donde cada una realiza lo siguiente:

1. Colorama: permite dar color al menú del cliente al loguearse en el chat.
2. Tqdm: facilita la construcción de la barra de "loading" previo a mostrar el menu de cada cliente.
3. Datetime: logra utilizar la hora del sistema para ser incluida en cada mensaje alojado en los log tanto de broadcast como de privado.