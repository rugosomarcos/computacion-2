#!/usr/bin/python3
import asyncio
import getopt
import sys
import datetime
import configparser
import os


try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'f:')

except:
    print('Missing arguments, re-run')
    sys.exit()

for (opt, arg) in opts:
    if opt == "-f":
        config_path = os.path.join(f'../{arg}')
        config = configparser.ConfigParser()
        config.read(config_path)
        host = config.get('DEFAULT', 'host')
        port = config.getint('DEFAULT', 'port')
        broadcast = config.get('DEFAULT', 'history_broadcast')
        private = config.get('DEFAULT', 'history_private')

    
clients = {}
async def handle_client(reader, writer):
    username = None
    accepted = False
    while True:
        actual_date = datetime.datetime.now()
        actual_date_str = actual_date.strftime("%d/%m/%Y %H:%M")
        data = await reader.read(1024)
        message = data.decode().strip()
        if not username:
            username = message
            while validate(username) == False and accepted == False:
                writer.write("Already exist, try with another".encode())
                data2 = await reader.read(1024)
                username = data2.decode().strip()
                if validate(username) == None:
                    accepted = True
                    continue
            clients[username] = (reader, writer, False, "")
            print(f"New client connected: {username}")
            log_broadcast("Server: ",f"New client connected -> {username}\n", actual_date_str)
            clients[username][1].write(f"Welcome {username}, type anything\n ".encode())
            continue
            
        if message == '/exit':
            clients[username][1].write("Disconnected".encode())
            del clients[username]
            break

        if len(message) > 0:
            if message.startswith("@"):
                if clients[username][2] == False:
                    try:
                        receiver, msg = message.split(None, 1)
                        receiver = receiver[1:]
                        if receiver in clients:
                            if clients[receiver][2] == False and receiver != username:
                                #Changing private flag state to True 
                                set_settings(username, receiver, True)
                                receiver_reader, receiver_writer, isInPrivate, userInPrivate = clients[receiver]
                                clients[username][1].write(f"Private room created with {clients[username][3]}, type anything... ".encode())
                                receiver_writer.write(f"Private room created with {userInPrivate}, type anything... ".encode())
                                receiver_writer.write(f"\n{userInPrivate} says : {msg}".encode())
                                log_private(username, receiver, msg, actual_date_str)

                                while True:
                                    try:
                                        await asyncio.create_task(handle_sender_side(clients[username][0], receiver_writer, username, receiver ))
                                    except KeyError:
                                        break
                            else:
                                clients[username][1].write("You cant chat with this user, try again later".encode())
                                continue
                    except:
                        clients[username][1].write("You have to enter the message to send".encode())
                        continue
                        

            try:
                if clients[username][2]:
                    if clients[username][3] in clients:
                        client = clients[username][3]
                        try:
                            if message != 'close' and len(message) > 0:
                                clients[client][1].write(f"{username} says: {message} ".encode())
                                log_private(username, client, message, actual_date_str)
                                
                            elif message == 'close':
                                clients[username][1].write("Private is closed".encode())
                                set_settings(username, client, False)
                                clients[client][1].write(f"Private is closed".encode())
                                continue
                        except asyncio.CancelledError:
                            continue
                if clients[username][2] == False:
                    log_broadcast(username, message, actual_date_str)
                    for client in clients:
                        if clients[client][2] == False:
                            
                            if len(message)>0 and message != 'close':
                                clients[client][1].write(f'{username} : {message}'.encode())
                        
                if message.startswith("/list"):
                    users = list_users(clients)
                    clients[username][1].write(f"\nCurrent users online : {users}".encode())
            except KeyError:
                break



async def handle_sender_side(client_reader, client_writer, sender, receiver ):
    
        while True:

            try:
                data = await client_reader.read(1024)
                message = data.decode().strip()
                actual_date = datetime.datetime.now()
                actual_date_str = actual_date.strftime("%d/%m/%Y %H:%M:%S")
            
                
                if clients[sender][2] and len(message) > 0:
                    if message == 'close':
                        set_settings(sender, receiver, False)
                        client_writer.write(f"Private is closed".encode())
                        clients[sender][1].write(f"Private is closed".encode())
                        await client_writer.drain()
                        continue
                    else: 
                        clients[clients[sender][3]][1].write(f"{sender} says : {message} ".encode())
                        log_private(sender, receiver, message, actual_date_str)
                else:
                    if message.startswith("/list"):
                        users = list_users(clients)
                        clients[sender][1].write(f"\nCurrent users online : {users}".encode())
                    if message == '/exit':
                        clients[sender][1].write("Disconnected".encode())
                        del clients[sender]
                    if message.startswith("@"):
                        receiver, msg = message.split(None, 1)
                        receiver = receiver[1:]
                        if clients[receiver][2] == False:
                            set_settings(sender, receiver, True)
                            clients[sender][1].write(f"Private room created with {receiver}, type anything... ".encode())
                            clients[receiver][1].write(f"Private room created with {sender}, type anything...".encode())
                            clients[receiver][1].write(f"\n{sender} says : {msg} ".encode())
                            log_private(sender, receiver, msg, actual_date_str)
                            while clients[sender][2] == True and clients[receiver][2] == True:
                                data = await client_reader.read(1024)
                                message = data.decode().strip()
                                if message == 'close':
                                    set_settings(sender, receiver, False)
                                    print(f"Sender -> {clients[sender][2]}, Receiver {clients[receiver][2]}")
                                    client_writer.write(f"Private is closed".encode())
                                    clients[sender][1].write(f"Private is closed".encode())
                                    
                                elif message != 'close' and clients[sender][2]:
                                    recv = clients[sender][3]
                                    clients[recv][1].write(f"\n{sender} says : {message} ".encode())
                                else:
                                    for client in clients:
                                        clients[client][1].write(f'\n{sender} : {message}'.encode())
                        else:
                            clients[sender][1].write("User is in private, try again later".encode())
                            continue
                    elif clients[sender][2] == False and message != '/list':
                        #broadcast
                        log_broadcast(sender, message, actual_date_str)               
                        for client in clients:
                            clients[client][1].write(f'\n{sender} : {message}'.encode())
                await client_writer.drain()
            
            except KeyError:
                break
        
           




def set_settings(sender, receiver, cond):
    sender_list = list(clients[sender])
    sender_list[2] = cond
    sender_list[3] = receiver
    clients[sender] = tuple(sender_list)
    receiver_list = list(clients[receiver])
    receiver_list[2] = cond
    receiver_list[3] = sender
    clients[receiver] = tuple(receiver_list)



def list_users(clients):
    list = []
    for user in clients:
        list.append(user)
    return list



def validate(username):
    if username in clients:
        return False
    else:
        return True



def log_broadcast(user, msg, date):
    file = open(f'../chat_records/{broadcast}', 'a')
    file.write(f'{date} - {user} : {msg}\n')
    file.flush()



def log_private(user1, user2, msg, date):
    file = open(f'../chat_records/{private}', 'a')
    file.write(f"\n{date} - {user1} -> {user2} : {msg}\n")
    file.flush()



async def main():
    server = await asyncio.start_server(handle_client, host, port)
    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')
    async with server:
        await server.serve_forever()


asyncio.run(main())

