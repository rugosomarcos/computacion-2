# TO-DO

En este archivo detallaremos funciones que pueden ser agregadas en un futuro, junto con recomendaciones de librerías investigadas por nosotros, por si se desea continuar con el desarrollo de este chat multiusuario:

1. Integración de una interfaz visual para el usuario y el servidor, el cual, por ejemplo puede realizarse de dos maneras:

    1.1- Siendo realizada para uso Desktop o de escritorio, con librerías como tkinter, por ejemplo.

    1.2- También puede realizarse para uso desde un navegador utilizando frameworks como flask, entre otros.

2. Funcionalmente se pueden agregar las siguientes optimizaciones:

    2.1- Permitir que cada usuario pueda chatear en más de una sala a la vez, como por ejemplo, que desee chatear en broadcast y en privado con distintos cliente al mismo tiempo. Es menester aclarar que, en este caso sería imprescindible la implementación de una interfaz gráfica, mencionada en el punto anterior.

    2.2- Incluir un sistema de registro y login para cada cliente/usuario, de manera que cada nick sea único para cada persona. Para esto se puede utilizar un wrapper de facil implementación llamada "Pyrebase", que facilita la interacción con la API de "firebase", construido especialmente para el lenguaje de programación python.
    
    2.3- Agregar base de datos en lugar de los archivos txt actuales. En este caso se puede utilizar también "Pyrebase" para implementar real-time-database.