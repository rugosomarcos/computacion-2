
# Proyecto final de la cátedra de Computación 2:



## Requisitos examen final

Para poder rendir el examen final es requisito haber regularizado la materia a fin del semestre.

El examen final consistirá en la exposición de un código integrador desarrollado por el alumno, cuyo tema será acordado previamente con el profesor.

Para definir los requisitos y alcances del trabajo, **antes de entrar a codear** será necesario plantear una arquitectura del proyecto, problema que solucionará, tareas que ejecutarían, entidades, herramientas de sincronismo concurrencia, justificaciones. Una vez que el **tema ha sido discutido y aprobado** para su desarrollo, se acordará un repositorio GIT donde se irán cargando los cambios periódicos en su desarrollo.

### Consideraciones del código:
El código debe solucionar un problema haciendo uso de las herramientas vistas en clase. Debe incluir aspectos como:
* Uso de Sockets con conexión de clientes múltiples de manera concurrente.
* Uso de mecanismos de IPC
* Uso de asincronismo de I/O
* Uso de cola de tareas distribuidas
* Parseo de argumentos por línea de comandos

Otros aspectos adicionales que puede considerarse:
* Despliegue en contenedores Docker
* Almacenamiento de datos en bases de datos
* Uso de Celery para resolver tareas en paralelo.
* Incorporación de entorno visual (web, desktop, etc).

### Definiendo los detalles del proyecto

Los detalles conceptuales del proyecto se deben presentar en el repositorio git de alumno.
Dentro de un directorio llamado **final/**. La intención es la de volcar las ideas a datos concretos de implementación. El repo del proyecto deberá contener un directorio **"doc/"** con la siguiente información: 

* Una descripción verbal de la aplicación (*Quiero armar una app cli-serv que haga esto y esto, donde el cliente envíe xx y el server haga xxxx, se usa concurrencia acá, se usa paralelismo acá, estas entidades se comunican de manera asincrónica usando xxxx, bla, bla*).
* Un gráfico de la arquitectura de la de la app (nodos, conectividad, mecanismos de IPC a implementar).
* Una lista de las funcionalidades de cada entidad (qué hacer el cliente, qué hace el servidor, qué hace cada elemento, etc.).

La intención es interactuar con los profesores para definir y depurar las propuestas, recortando funcionalidad si el proyecto es demasiado extenso, añadiendo funcionalidades si es muy simple, etc. Los cambios de requisitos deberán siempre *commitearse* al repo git.

### Desarrollo

Una vez que la aplicación a desarrollar sea **aprobada** con el profesor de la cátedra, podrá comenzar el desarrollo de la misma.
Durante el desarrollo de la app deberán realizarse commits periódicos en el repo, que reflejen los cambios y progreso del desarrollo.
Asimismo, deberán consultarse los inconvenientes que surjan durante el desarrollo, con la intención de ajustar detalles de los requisitos planteados inicialmente (*doc*).
Una vez desarrollada la app, deberá ser presentada al profesor al menos una semana antes de la mesa de examen final (si puede ser antes mejor, para evitar cambios de ultimo momento), para que éste pueda visarla y permitirle inscribirse a la mesa.

Deberán añadirse algunos archivos markdown adicionales en el repositorio git del proyecto para poder presentarlo en el examen:
* **INSTALL.md**: contendrá las instrucciones para clonar e instalar/lanzar la aplicación, o despliegue de la misma.
* **README.md**: contendrá la ayuda y uso básico de la aplicación.
* **INFO.md**: contendrá un breve informe sobre las decisiones principales de diseño del sistema, y su justificación (ej, por qué un determinado modelo de datos, o tipo de almacenamiento, o uso de multiproceso/multithread, etc).
* **TODO.md**: contendrá una lista detallada de mejoras y posibles nuevas características/features del sistema para futuras versiones.

### Examen final

Durante la presentación, el alumno expondrá el funcionamiento de la aplicación, explicará el código fuente y tecnologías utilizadas, y el tribunal de la mesa podrá:
* Realizar preguntas teóricas sobre los contenidos prácticos expuestos por el alumno.
* Pedir justificaciones de los mecanismos utilizados (*por qué XX y no YY, etc.*)
* Solicitar pequeños *fixes* de bugs detectados durante la presentación, y pequeñas modificaciones del código.

### Notas:

* No tiene fecha **límite de presentación** (mientras no se les venza la regularidad de la materia :P ). Pueden presentarlo a lo largo del año. El único requisito es que, una vez que decidan en qué mesa van a rendir, más tardar una semana antes de la mesa deben tener el código completo y aprobado. Se pueden definir los requisitos de la app, realizar el desarrollo y aprobarlo en cualquier momento, y dejar el código listo para ser presentado en la siguiente mesa disponible.

* El código deberá ser desarrollado de **manera incremental**, por lo que se valoran cambios progresivos (*commits*) en el repositorio de software durante todo el proceso de desarrollo. En la medida que vayan desarrollando la app, vayan commiteando seguido, no esperen a tener una versión funcional para commitear, no importa si funciona o no, commiteen igual! Eso les sirve para que podamos charlar dificultades, inconvenientes, para que yo pueda ver la evolución del proyecto, y de paso, a ustedes les queda un backup de todo lo que van haciendo (ya no es excusa el "se me rompió el disco, me robaron la compu y tenía todo ahí, etc... tenemos git, aprovechenlo!).

* La idea de ese **doc/** en el repositorio es que podamos quedar con una idea fina de lo que van a hacer. A mi me sirve para saber que tienen noción de los alcances de sus apps, lo que tienen que armar, y a ustedes les sirve de guía para no *irse por las ramas* y acotarse a eso. Igualmente durante el desarrollo pueden charlarse cambios, pero lo ideal es que quede lo más definido posible al principio.


# Resolución

## Introducción

El proyecto trata acerca de la construcción de un chat multicliente en el lenguaje python, utilizando la librería asyncio para manejar los flujos de datos.
Cuenta con dos archivos que permiten la ejecución del servidor y de los clientes.

Para ejecutar tanto el servidor como el cliente, se debe abrir una terminal para el servidor, y tantas terminales como clientes se deseen conectar al chat. Para ello, inicialmente se debe situar en la carpeta del proyecto y ejecutar el siguiente comando:

```
    python3 server.py -f config.env

```

```
    python3 client.py -f config.env
```

Dentro del archivo config.env tendremos disponibles las configuraciones pertinentes a el host y el puerto en el cual se desea operar. 

Al correr el servidor, en la consola se visualizará lo siguiente:

## Terminal del servidor

![Server](./img/server-init.png)

Vemos en la terminal que el servidor se encuentra corriendo en los pares que indican el ip y el puerto definido en el archivo config.env, para establecer la comunicación.
Por cada cliente ejecutado en su respectiva terminal, se le solicita que ingrese un nick, el cual será validado por el servidor, esta validación consiste en recorrer los nicks ya conectados en el chat, en caso de que ya exista un usuario con ese nombre, deberá ingresar nuevamente hasta que este sea válido.
Vemos a continuación una imagen de este caso:

![Validate nickname](./img/validate-nick.png)

En esta situación, ya se encuentra un usuario logueado en el sistema con el nombre "marcos", por lo que el servidor le solicita al cliente que ingrese otro nick, y al ingresar un nick válido, le permite utilizar el chat.
Una vez ingresado, podrá solicitar al servidor que retorne una lista de los usuarios conectados tipeando /list. Tal como lo vemos aquí:

![List users](./img/list-users.png)


En caso de que desee chatear en privado con otro usuario deberá ingresar @nick mensaje , siendo nick el nickname del receptor con el cual desea hablar por privado y el mensaje a enviar.
El servidor realizará validaciones para permitir o no abrir la sala del privado.
Para cerrar la conexión, se puede realizar desde ambas terminales ingresando la palabra close.
En este ejemplo el cliente "marcos" intentará comunicarse con el cliente "lucas" para iniciar un chat privado


## Cliente "marcos"

![Cliente marcos](./img/cliente-marcos.png)


## Cliente "lucas"

![Cliente lucas](./img/cliente-lucas.png)



Cada cliente puede cerrar el chat ingresando "close", donde el servidor cerrará la sala privada de cada usuario y le permitirá hablar en la sala general, es decir en broadcast.

























