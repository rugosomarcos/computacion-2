#!/usr/bin/python3

import multiprocessing
import threading
import sys
import os


def stdin_reader(s):
    sys.stdin = os.fdopen(0)
    print("Write a message or press CTRL+Z to exit: ")
    while True:
        try:
            msg = input()
            s.send(msg)
        except EOFError:
            print("Exit")
            break


def pipe_reader(p):
    while True:
        msg = p.recv()
        print('Reading from (pid: %d): %s' % (threading.get_ident(), msg))
        print("Write a message or press CTRL+Z to exit: ")


def main():
    s, p = multiprocessing.Pipe()
    p1 = threading.Thread(target=stdin_reader, args=(s, ))
    p2 = threading.Thread(target=pipe_reader, args=(p, ))
    p1.start()
    p2.start()

    p1.join()
    p2.join()


if __name__ == '__main__':
    main()


