#!/usr/bin/python3

import signal
import os
import time as t


def child_1():

    for i in range(10):
        os.kill(os.getppid(), signal.SIGUSR1)
        print("Im the first child - PID: ", os.getpid(), ": ping")
        t.sleep(3)


def child_2():
    for i in range(10):
        signal.pause()
        print("Im the second child - PID: %d pong " % (os.getpid()))


def handler(s, f):
    pass


def main():
    print("\n-Im the father creating 2 process: \n  ")
    signal.signal(signal.SIGUSR1, handler)
    pid_1 = os.fork()
    if pid_1 == 0:
        child_1()

    pid_2 = os.fork()
    if pid_2 == 0:
        child_2()

    for i in range(10):
        signal.pause()
        os.kill(pid_2, signal.SIGUSR1)


if __name__ == "__main__":
    main()
