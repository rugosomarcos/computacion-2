#!/usr/bin/python3

import sys
import os
import signal
import getopt
import time

def child_handler(sig, frame):
    print("Im child´s PID ", os.getpid(), ", i receive signal SIGUSR2", sig, "from my father- PID: ", os.getppid())


def main():


    (opts, args) = getopt.getopt(sys.argv[1:], 'p:', ['process='])

    print("Generating", sys.argv[2], "Child´s")
    for op, ar in opts:
        if op == "-p" or op == "--process":
            child = int(ar)
            for i in range(child):
                pid = os.fork()
                if pid == 0:
                    signal.signal(signal.SIGUSR2, child_handler)
                    signal.pause()
                    exit(0)
                else:
                    time.sleep(0.1)
                    print("Creating process- PID: ", os.getpid())
                    os.kill(pid, signal.SIGUSR2)
                    os.wait()


if __name__ == '__main__':
    main()
