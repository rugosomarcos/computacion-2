#!/usr/bin/python3
import asyncio
import re


async def tcp_client():
    reader, writer = await asyncio.open_connection(
        '127.0.0.1', 8891)

    msg = input("> ")
    print(f'Send: {msg!r}')
    writer.write(msg.encode())
    await writer.drain()

    data = await reader.read(100)
    format_data = data.decode()
    format_data = re.sub('\s+', ' ', format_data)
    print(f'Received:\n {format_data}')

    
    await writer.wait_closed()

asyncio.run(tcp_client())
