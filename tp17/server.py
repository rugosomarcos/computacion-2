#!/usr/bin/python3
import asyncio
import subprocess


async def handle(reader, writer):
    data = await reader.read(100)
    command = data.decode()
    addr = writer.get_extra_info('peername')

    print(f"Command received: {command!r} from {addr!r}")

    process = subprocess.Popen([command], shell=True, universal_newlines=True, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    if process.returncode == 0:
        if stdout != 0:
            msg = stdout
            writer.write(msg.encode('utf-8'))
        elif stdout == 0:
            msg = stderr
            writer.write(msg.encode('utf-8'))
    else:
        msg = stderr
        writer.write(msg.encode('utf-8'))

    await writer.drain()

    writer.close()

async def main():
    server = await asyncio.start_server(
        handle, '127.0.0.1', 8891)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

asyncio.run(main())
