#!/usr/bin/python

import multiprocessing
import os
import time


def c(p, q):
    print("Process %d - PID %d" % (p, os.getpid()))
    time.sleep(p)
    q.put(os.getpid())


def main():
    q = multiprocessing.Queue()
    child = []
    print("Queue:")
    for i in range(10):
        proc = multiprocessing.Process(target=c, args=(i+1, q,))
        child.append(proc)
        child[i].start()
        child[i].join()

    while q.empty():
        print(q.get())


if __name__ == '__main__':
    main()







