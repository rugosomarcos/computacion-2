#!/usr/bin/python3

import socket
import getopt
import sys

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'p:t:f:')


except socket.error:
    print('Failed creating the socket')
    sys.exit()


def main():

    for (opt, arg) in opts:
        if opt == "-p":
            port = int(arg)
        elif opt == "-f":
            f = arg
        elif opt == "-t":
            protocol = arg
    while True:

        if protocol == 'tcp':

            print("-------- SELECTED PROTOCOL:TCP... Waiting for messages --------\n")
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            host = ""
            s.bind((host, port))
            s.listen(5)
            client_socket, address = s.accept()
            while True:
                msg = client_socket.recv(1024)
                file = open(f, 'a')
                file.write(msg.decode("ascii"))
                print("Message received, stored in:", f)
                file.flush()

        elif protocol == 'udp':

            print("\n-------- SELECTED PROTOCOL:UDP... Waiting for messages --------\n")
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            host = ""
            s.bind((host, port))
            while True:
                data, address = s.recvfrom(2048)
                file = open(f, 'a')
                file.write(data.decode('ascii'))
                print("Message received, stored in:", f)
                file.flush()



if __name__ == '__main__':
    main()
