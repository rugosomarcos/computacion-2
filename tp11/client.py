#!/usr/bin/python3

import socket
import getopt
import sys


try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'a:p:t:')


except socket.error:
    print('Failed creating the socket')
    sys.exit()

while True:
    for (opt, arg) in opts:
        if opt == "-a":
            host = arg
        elif opt == "-p":
            port = int(arg)
        elif opt == "-t":
            protocol = arg


    if protocol == 'tcp':

        print("\n-------- SELECTED PROTOCOL:TCP --------\n")
        print("Send a message:\n")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print(host)
        host = ""
        s.connect((host, port))
        for line in sys.stdin:
            s.send(line.encode())

    elif protocol == "udp":

        print("\n-------- SELECTED PROTOCOL:UDP --------\n")
        print("Send a message:\n")
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        host = ""
        s.connect((host, port))
        for line in sys.stdin:
            s.send(line.encode())




