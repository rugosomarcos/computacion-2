----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ENUNCIADO: Lea, ejecute e interprete el código chained.py del repositorio git de la cátedra.

Escriba su interpretación de qué es lo que hace dicho código en un archivo llamado chained.md en su repositorio git. Detalle lo más posible el funcionamiento del código

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

RESOLUCIÓN:


SE IMPORTAN 3 LIBRERÍAS:

.asyncio : Para aplicar mecanismos propios del asincronismo
.random : Para generar valores aleatorios
.time : Para realizar operaciones con respecto al tiempo

SE DEFINEN 4 FUNCIONES COROUTINES:

primera y segunda: Comienza generando un número aleatorio entre 0 y 10 y lo utiliza para luego implementar asyncio.sleep , a través de un await, de esta manera el sistema permite la ejecución de otras tareas mientras espera a que termine esta primera. Luego realiza un cálculo para determinar cuantos segundos tardó en ejecutarse , el cual muestra con un print (f"Retornando primera({n}) == {result}.")

chain: comienza iniciando un contador de tiempo a través de una función importada de la librería time [time.perf_counter()], luego llama a las funciones primera y segunda para ser lanzadas de manera asincrónica, finalmente devuelve la cantidad de segundos que tardó en ejecutarse

main: permite la ejecución de las coroutines de manera concurrente a través del método gather(), importado desde la librería asyncio.


