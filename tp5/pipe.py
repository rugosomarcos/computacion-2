#!/usr/bin/python3

import os
import signal
import time


def handler(sig, frame):
    pass


def main():

    a = os.getpid()
    r, w = os.pipe()
    signal.signal(signal.SIGUSR1, handler)

    b = os.fork()
    if not b:
        c = os.fork()

    if os.getpid() == a:
        time.sleep(1)
        os.close(w)
        r = os.fdopen(r)
        os.kill(b, signal.SIGUSR1)
        signal.pause()
        print("A- (PID: %d) READING..." % os.getpid())
        print(r.read())

    elif not b and c:
        signal.pause()
        os.close(r)
        w = os.fdopen(w, 'w')
        w.write("Message 1: (PID: %d)\n" % os.getpid())
        time.sleep(1)
        os.kill(c, signal.SIGUSR1)

    elif not c:
        signal.pause()
        os.close(r)
        w = os.fdopen(w, 'w')
        w.write("Message 2: (PID: %d)" % os.getpid())
        os.kill(a, signal.SIGUSR1)


if __name__ == '__main__':
    main()
