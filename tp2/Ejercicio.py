#!/usr/bin/python3

import getopt
import sys
import subprocess as sp
from datetime import datetime


def main():
    try:
        (opt, arg) = getopt.getopt(sys.argv[1:], 'c:f:l:')
        print('OPTIONS  :', opt)
        for op, ar in opt:
            if op == '-c':
                command = ar
            elif op == '-f':
                output_file = ar
            elif op == '-l':
                log_file = ar
            else:
                print("INVALID INPUT")

        with sp.Popen([command], stdout=sp.PIPE, stderr=sp.PIPE, universal_newlines=True, shell=True) as process:
            out, err = process.communicate()

        out_file = open(output_file, "w")
        log_file = open(log_file, "w")

        if process.returncode == 0:
            out_file.write(str(datetime.now())+" -COMMAND -> " + command + out)
            log_file.write(str(datetime.now())+" - " + command + "- EXECUTED CORRECTLY \n")
        else:
            log_file.write(str(datetime.now())+" COMMAND " + err)

    except getopt.GetoptError as e:
        print("ERROR", str(e))
        sys.exit(1)


if __name__ == '__main__':
    main()
