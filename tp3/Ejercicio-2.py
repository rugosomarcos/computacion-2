#!/usr/bin/python3

import getopt
import sys
import os



def child():

    print("Im process PID : ", os.getpid(), ", my father´s PID is:", os.getppid())
    os._exit(0)


def main():

    fork_childs = 0

    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'n:')
        print("Generating", sys.argv[2], "Childrens")
        for op, ar in opts:
            if op == '-n':
                fork_childs = int(ar)

        for i in range(fork_childs):
            child_fork = os.fork()
            if child_fork == 0:
                child()

    except getopt.GetoptError as e:
        print("ERROR", str(e))
        sys.exit(0)


if __name__ == '__main__':
    main()
