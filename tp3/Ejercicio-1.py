#!/usr/bin/python3

import os


def main():
    pid = os.fork()
    if pid == 0:
        for i in range(5):
            print("Im the child, my PID is:", os.getpid())
        print("PID", os.getpid(), "Ending")
    else:
        for i in range(2):
            print("Im the father, my PID is: ", os.getpid(), ", my child´s PID is", pid)
        os.wait()
        print("My child´s process ", pid, "ended")


if __name__ == '__main__':
    main()
